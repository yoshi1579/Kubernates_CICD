import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import './aboutUs.css'

export default class aboutUs extends Component {
  render() {
    return (
        <div class="text-center" id="bg">
            <div  id="body_box">
                <h1 class="cover-heading">Find your Partner</h1>
                <p class="lead">From the list of adaptable pets, save your Partner.</p>
                <p>Find a vet to take care of your pet and donate money to local animal shelters!</p>
                <p class="lead">
                <a href="#" class="btn btn-lg btn-secondary">Learn more</a>
                </p>
            </div>
        </div>
    )
  }
}
