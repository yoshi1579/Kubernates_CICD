import React, { Component } from 'react'

export default class EntityAttribute extends Component {
  render() {
    return (
      <div>
        <h3><b>{this.props.attrKey.charAt(0).toUpperCase() + this.props.attrKey.slice(1)}</b></h3>
        <p>{this.props.attrValue}</p>
      </div>
    )
  }
}
