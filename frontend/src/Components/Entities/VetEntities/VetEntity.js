import React, { Component } from 'react';
import './VetEntity.css';
import { connect } from 'react-redux';
import { fetchPetEntity } from '../../../actions/dataModelAction';
import EntityAttribute from '../EntityAttribute/EntityAttribute';
import RatingStar from '../../RatingStar/RatingStar';
import Review from '../Reviews/Review';

// import Card from './Card';


class VetEntity extends Component{

    componentWillMount(){
        this.props.fetchPetEntity(this.props.match.params.vetId, 'vet');
    }

    render(){
        let info = this.props.petInfo;
        let hours = []
        for(let hour in info.hours){
            hours.push(<li>{info.hours[hour]}</li>);
        }

        
    
        return (
            <div>     
                <div id="myCarousel" className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img className="first-slide" src={info.img} alt="First slide"/>
                        <div className="container">
                        <div className="carousel-caption">
                            <h1>{info.name}</h1>
                            {/* <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> */}
                            {/* <p><a className="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p> */}
                        </div>
                        </div>
                    </div>
                    <div className="carousel-item">
                        <img className="second-slide" src={info.img} alt="Second slide"/>
                        <div className="container">
                        <div className="carousel-caption">
                            <h1>Location:</h1>
                            <p>{info.location}</p>
                            {/* <p><a className="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p> */}
                        </div>
                        </div>
                    </div>
                    <div className="carousel-item">
                        <img className="third-slide" src={info.img} alt="Third slide"/>
                        <div className="container">
                        <div className="carousel-caption">
                            <h1>Hours:</h1>
                            <p>{hours}</p>
                            {/* <p><a className="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p> */}
                        </div>
                        </div>
                    </div>
                    </div>
                    <a className="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                    </a>
                </div>




            
                <main role="main" className="container">
                    <div className="row">
                        <div className="col-md-8 blog-main">
                            <div className="row mb-2">
                                <div className="col-md-12">
                                    <div className="card flex-md-row mb-4 box-shadow">
                                        <div className="card-body d-flex flex-column align-items-start">
                                            <h2 className="d-inline-block mb-2 text-primary"><b>About</b></h2>
                                    
                                            <div className="mb-1 text-muted">9 miles away</div>
                                            <div className="mb-1 text-black">{info.name}</div>
                                            <div className="mb-1 text-black">{info.location}</div>
                                            <div className="mb-1 text-black">{info.contact}</div>
                                            <div className="mb-1 text-black"><b>Hours</b></div>
                                            <div className="mb-1 text-black">{hours}</div>
                                            <br/>
                                            
                                            <img className="" src="http://www.destination360.com/north-america/us/texas/austin/northcross-suites-austin-nw-map.gif" alt="Third slide"/>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                        
                            </div>

                            <Review reviewName={info.reviews}/>
                            <nav className="blog-pagination">
                                <a className="btn btn-outline-primary" href="">Previous Vet</a>
                                <a className="btn btn-outline-secondary disabled" href="">Next Vet</a>
                            </nav>

                        </div>

                        {/* <aside className="col-md-4 blog-sidebar">
                            <div className="p-3 mb-3 bg-light rounded">
                                <h4 className="font-italic">About</h4>
                                <p className="mb-0">Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
                            </div>

                            <div className="p-3">
                                <h4 className="font-italic">Archives</h4>
                                <ol className="list-unstyled mb-0">
                                <li><a href="#">March 2014</a></li>
                                <li><a href="#">February 2014</a></li>
                                <li><a href="#">January 2014</a></li>
                                <li><a href="#">December 2013</a></li>
                                <li><a href="#">November 2013</a></li>
                                <li><a href="#">October 2013</a></li>
                                <li><a href="#">September 2013</a></li>
                                <li><a href="#">August 2013</a></li>
                                <li><a href="#">July 2013</a></li>
                                <li><a href="#">June 2013</a></li>
                                <li><a href="#">May 2013</a></li>
                                <li><a href="#">April 2013</a></li>
                                </ol>
                            </div>

                            <div className="p-3">
                                <h4 className="font-italic">Elsewhere</h4>
                                <ol className="list-unstyled">
                                <li><a href="#">GitHub</a></li>
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">Facebook</a></li>
                                </ol>
                            </div>
                        </aside> */}

                    </div>

                </main>
                <div className="row mb-2">
                    <div className="col-md-6">
                    <div className="card flex-md-row mb-4 box-shadow h-md-250">
                        <div className="card-body d-flex flex-column align-items-start">
                        <strong className="d-inline-block mb-2 text-primary">Vets</strong>
                        <h3 className="mb-0">
                            <a className="text-dark" href="">Emancipet</a>
                        </h3>
                        <div className="mb-1 text-muted">9 miles away</div>
                        <RatingStar/>
                        <br/>
                        <p className="card-text mb-auto">“Emancipet offers No Cost and Low Cost spay and neutering services.”</p>
                        <a href="">See all reviews</a>
                        </div>
                        <img className="card-img-right flex-auto d-none d-lg-block"  src="https://s3-media3.fl.yelpcdn.com/bphoto/wLB-AA3Dt15HeHs1OOxGGQ/ls.jpg" alt="Card image cap"/>
                    </div>
                    </div>
                    <div className="col-md-6">
                    <div className="card flex-md-row mb-4 box-shadow h-md-250">
                        <div className="card-body d-flex flex-column align-items-start">
                        <strong className="d-inline-block mb-2 text-success">Shelter</strong>
                        <h3 className="mb-0">
                            <a className="text-dark" href="">Austin Dog Rescue</a>
                        </h3>
                        <div className="mb-1 text-muted">3 miles away</div>
                        <RatingStar/>
                        <br/>
                        <p className="card-text mb-auto">Austin, TX 78798 Downtown</p>
                        <p className="card-text mb-auto">Contact: (512) 827-9787</p>

                        <a href="">More Detail</a>
                        </div>
                        <img className="card-img-right flex-auto d-none d-lg-block" src="https://s3-media4.fl.yelpcdn.com/bphoto/OlIVz1LAijuaOs58mItYJA/o.jpg" alt="Card image cap"/>
                    </div>
                    </div>
                </div>
            </div>


        );
    }
}

const mapStateToProps = state => ({
    petInfo: state.petInfo.items
})

export default connect(mapStateToProps, {fetchPetEntity})(VetEntity)
