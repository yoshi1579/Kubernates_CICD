import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './Footer.css';

class Footer extends Component{
  render(){
    return (
      <footer className="text-white default bg-dark">
        <div className="container">
          <p className="float-right">
            <Link to="#">Back to top</Link>
          </p>
          <p>&copy; ConnectPetsTo</p>
        </div>
      </footer>
    )
  }
}

export default Footer
