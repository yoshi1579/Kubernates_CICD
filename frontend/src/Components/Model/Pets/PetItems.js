import React, { Component } from 'react'
import { Link } from 'react-router-dom'


export default class PetItems extends Component {
  render() {
    //let info = this.props.petData.id;
    return (
    <div className="col-md-4">
        <div className="card mb-4 box-shadow">
          <Link to={"/Pets/PetEntity/".concat(this.props.petData.id)}><img className="card-img-top" src={this.props.petData.image} alt="Card cap"/></Link>
          <div className="card-body">
          <Link  to={"/Pets/PetEntity/".concat(this.props.petData.id)}><p className="card-text">{this.props.petData.name}</p></Link>
          {/*<small className="text-muted">9 miles away</small>*/}
            <div className="d-flex justify-content-between align-items-center">
              <div className="btn-group">
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.petData.age}</button>
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.petData.breed}</button>
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.petData.sex}</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
