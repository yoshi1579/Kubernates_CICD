import React, {Component} from 'react';
import {connect} from 'react-redux';
import PetItems from './PetItems'
import { fetchData } from '../../../actions/dataModelAction';
import './Pets.css';

class Pets extends Component{

  // TO DO LIST
  // fetch data from backend based
  // this.props.petType will indicate either dog or cat etc
  // this.props.petType will be used for filtering

  // this is getting hard coded static data for phase 1
  // check dataModelAction.js
  componentWillMount(){
    this.props.fetchData('pet');
  }

  render(){
    let petItems = this.props.pets.map(pet => {
      return(
        <PetItems petData = {pet}/>
      );
    });
   
    return (
      <div className="row">
        {petItems}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  pets: state.pets.items
});

export default connect(mapStateToProps, { fetchData })(Pets)
