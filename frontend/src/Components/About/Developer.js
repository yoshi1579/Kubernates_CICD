import React, { Component } from 'react'
import fetchData from './StaticData'
import Issue from './Issue'
import './Developer.css';

export default class Developer extends Component {
  constructor(props) {
    super(props);
    this.state= {
      devInfo: [],
      count: 0,
    }
  }

  componentWillMount() {
    /* TODO: have static user info and only generate issues/commits dynamically */
    fetch ('https://gitlab.com/api/v4/projects/7164816/repository/contributors', {
    }).then(res => res.json())
    .then(jsonArray => {
      var skip = [];
      for(var i in jsonArray)
      {
        let developerInfo = fetchData(jsonArray[i].email);
        if (developerInfo === 'missing')
        {
          skip.push(i);
          continue;
        }
        jsonArray[i].name = developerInfo.name;
        jsonArray[i].bio = developerInfo.bio;
        jsonArray[i].image = developerInfo.image;
        jsonArray[i].userId = developerInfo.userId;
        this.props.incrementCommits(jsonArray[i].commits);
      }
      for (var each of skip){
        jsonArray.splice(each, 1);
      }
      return jsonArray;
    })
    .then(data => this.setState({devInfo: data}));
  }


  render(){
    return this.state.devInfo.map(dev => (
      <div className="col-md-4 d-flex align-items-stretch" key={dev.name}>
        <div className="card mb-4 box-shadow">
          <div className="card-body">
            <img className="card-img-top" src={dev.image} alt="Card cap"/>
            <h5 className="card-title">{dev.name}</h5>
            <p className="card-text">{dev.bio}</p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">Commits: {dev.commits}</li>
            <Issue user={ dev.userId } increment= { this.props.incrementIssues.bind(this) }/>
          </ul>  
        </div>
      </div>
      )
    )
  }
}
