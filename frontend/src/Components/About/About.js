import React, { Component } from 'react'
import Developer from './Developer'
import Tools from './Tools'

export default class About extends Component {
  constructor(props) {
    super(props);
    this.state= {
      'total_issue_count': 0,
      'total_commit_count': 0,
    }
  }

  incrementCommits(commit_count) {
    var commit = this.state.total_commit_count;
    commit += (commit_count);
    this.setState({total_commit_count: commit});
  }

  incrementIssues(issue_count) {
    var issue = this.state.total_issue_count;
    issue += (issue_count);
    this.setState({total_issue_count: issue});
  }

  render(){
    return (
      <div className="page-header">
        <div className="album py-5 bg-light">
          <div className="container">
            <div className="container">
              <div className="row row-offcanvas row-offcanvas-right">
                <div className="col-12 col-md-6">
                  <div className="jumbotron">
                    <h1>About the Site</h1>
                    <p>
                      ConnectPetsToMe is a site that allows prospective pet owners to find pets they may be interested in adopting,
                       nearby veterinary offices to vaccinate and care for their pet after adoption,
                       and animal shelters where visitors may volunteer at or donate to. 
                    </p>
                  </div>
                </div>
                <div className="col-md-6 col 0">
                  <div className="jumbotron">
                      <h1>About our Data</h1>
                      <p>
                        Information on individual instances of pets, vets, and shelters was scraped manually from <a href="https://www.petfinder.com/">Petfinder</a>
                        , <a href="https://developers.google.com/maps/documentation/">Google</a>, 
                        and <a href="https://charity.3scale.net/docs/data-api/reference">CharityNavigator</a>, respectively. 
                        RESTful API documentation for our site is available <a href="https://documenter.getpostman.com/view/4693218/RWEjqHim"> here</a>,
                         and repository is available <a href="https://gitlab.com/CS373-18SU-GRP/IDB">here</a> .
                      </p>
                    </div>
                  </div>
              </div>
            </div>
            <h1 className="text-center">About the ConnectPetsToMe Team</h1>
            <div className="row">
              <Developer incrementCommits={ this.incrementCommits.bind(this) } incrementIssues = { this.incrementIssues.bind(this) }/>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-12 col-sm-3">
                  <ul className="list-group">
                    <li className="list-group-item">Total Commits: {this.state.total_commit_count} </li>
                    <li className="list-group-item">Total Issues: {this.state.total_issue_count} </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="album py-5 bg-light">
              <h1 className="text-center">Tools Used</h1>
              <div className="container">
                <div className="row">
                  <Tools name="React.js" description="Javascript library used to create UI components."/>
                  <Tools name="Redux" description="State manager used for ease of communicating between UI components."/>
                  <Tools name="React Router" description="React extension used to manage application states."/>
                  <Tools name="Bootstrap" description="CSS framework used to style web pages."/>
                  <Tools name="Postman" description="API design tool used to create our website API."/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
};
