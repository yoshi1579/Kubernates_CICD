import {combineReducers} from 'redux';
import dataModelReducer from './dataModelReducer';
import petInfoReducer from './petInfoReducer';

export default combineReducers({
    pets: dataModelReducer,
    vets: dataModelReducer,
    shelters: dataModelReducer,
    petInfo: petInfoReducer,
});
