import {FETCH_DATA, FETCH_PET_ENTRY} from './type';

// TODO LIST
// fetch data from backend 
// use axios to send rest api request 

export function fetchPetEntity(Id, dataType){
    let petsInfo = [
        {name: "Charger", breed:"Siamese", img: "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/38345765/2/?bust=1496895722", location:"Austin, TX", age: "Young", sex:"male", health:"Spayed / neutered.", meet:"We know what you're really thinking is: Does he bring all the boys to the yard? Hmmm. Well, that remains to be seen but Milkshake sure is a sweetie. This little guy came to us with a curious sort of medical condition--a severe headtilt. Multiple vet visits have not revealed the cause, but with daily massage, the head tilt has gotten much less pronounced. This little guy is adorable and charming. He's also heartworm positive which will be treated at CSRA expense!"},
        {name: "June 2", breed:"Domestic Short Hair", img:"https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/41998452/3/?bust=1529884412", location:"Austin, TX", age: "Young", sex:"female", meet:"Please contact Beth (bethbach2@gmail.com) for more information about this pet.June is active, friendly, and talkative. Very outgoing and loves wrestling with her brothers. Super loud purr that she turns on frequently! One of her brothers is a Lynx Point so there's Siamese in there! 6/26/18 9:37 PM"},
        {name: "Shaggy", breed:"Shih Tzu", img: "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/42022417/1/?bust=1530117339", location:"Austin, TX", age: "Senior", sex:"male", health:"Vaccinations up to date, spayed / neutered.", meet:"14 years old"}
        
    ];

    let vetsInfo = [
        {name: "Emancipet", hours: [
        "Mon	10:00 am - 6:00 pm",
        "Tue	10:00 am - 6:00 pm",	
        "Wed	10:00 am - 6:00 pm",	
        "Thu	10:00 am - 6:00 pm",
        "Fri	10:00 am - 6:00 pm",	
        "Sat	10:00 am - 6:00 pm",	
        "Sun	close"], contact:"(512) 587-7729", img: "https://s3-media3.fl.yelpcdn.com/bphoto/wLB-AA3Dt15HeHs1OOxGGQ/ls.jpg", location:"7201 Levander Lp Bldg I & K Austin, TX 78702", reviews: {name:"Barbarita R.", img: "https://s3-media3.fl.yelpcdn.com/photo/Bqr-yYNp2amFLgw_1xMSfQ/60s.jpg", rating: '5', comment: "I rescued a 2 yo 70 lb. male lab and made an appointment to get him neutered before picking him up 3 weeks ago. Unfortunately, the first available appointment was July 31. After I injured my wrist and ribs trying to control him around other dogs I emailed them and asked to be put on a wait list for any cancellations. They called me the next day and told me to bring him in the day after that. We got there about 10 minutes before they opened and there were 3 people waiting. Once they checked us in I explained that he was hyper-reactive to other dogs and they sent a tech out to get him and make sure the other dogs were moved away while they brought him in"}},
        {name: "Zippivet", hours: [
            "Mon	7:00 am - 7:00 pm",
            "Tue	7:00 am - 7:00 pm",	
            "Wed	7:00 am - 7:00 pm",	
            "Thu	7:00 am - 7:00 pm",
            "Fri	7:00 am - 7:00 pm",	
            "Sat	9:00 am - 5:00 pm",	
            "Sun	9:00 am - 5:00 pm"], contact:"(512) 904-0218", img:"https://s3-media1.fl.yelpcdn.com/bphoto/S2LYJ96acZeO62hfcfzHjQ/ls.jpg", location:"10721 Research Blvd Ste A-110 Austin, TX 78759", reviews: {name: "Ashleigh F.", img:"https://s3-media3.fl.yelpcdn.com/photo/sjUGHFAXJibET8bXLQPx1w/60s.jpg", rating: '4', comment:"I came here today out of desperation with my dog, Babs, since my regular vet was booked. I have never been more thankful for a vet that couldn't accommodate us because it sent me to ZippiVet! There was only one spot available that I was immediately booked into and the tech apologized that it couldn't be sooner. It wasn't her fault, obviously, but it was so touching for someone to be so caring. She even gave me options such as going to Kyle since they had more openings. I opted to take the spot they had here and a few hours later I got a call that there was a cancellation if I wanted to come in earlier. I didn't even ask to be on a cancellation list or if they had one so again I was impressed! "}},
        {name: "PAZ Veterinary", hours: [
            "Mon	8:00 am - 6:00 pm",
            "Tue	8:00 am - 6:00 pm",	
            "Wed	8:00 am - 6:00 pm",	
            "Thu	8:00 am - 6:00 pm",
            "Fri	8:00 am - 6:00 pm",	
            "Sat	9:00 am - 5:00 pm",	
            "Sun	close"], contact:"(512) 236-8000", img: "https://s3-media4.fl.yelpcdn.com/bphoto/MvDi_2SFJk8WYtCdRCnIEA/o.jpg", location:"2400 E Cesar Chavez St Ste 100 Austin, TX 78702", reviews: {name: "Amanda R.", img: "https://s3-media4.fl.yelpcdn.com/photo/LBWPvb3UDLzisj-yPdxR4Q/60s.jpg", rating: '5', comment: "This is by far the best place in town. The staff really cares about your pet as if he/she were their own. My dog recently received a bilateral TPLO and I did not expect such a wonderful experience during such a difficult time. I received a call from the surgeon, Dr. Might, before and after the procedure, and he was more than happy to explain his plan in detail and answer all my questions/concerns. After his surgery, I received a text from the staff with a picture of my dog, reassuring me that he was doing well. "}}
        ,{name: "Austin Urban Vet Center", hours: [
            "Mon	7:00 am - 6:00 pm",
            "Tue	7:00 am - 6:00 pm",	
            "Wed	7:00 am - 6:00 pm",	
            "Thu	7:00 am - 6:00 pm",
            "Fri	7:00 am - 6:00 pm",	
            "Sat	9:00 am - 5:00 pm",	
            "Sun	close"], contact:"(512) 904-0218", img:"https://s3-media1.fl.yelpcdn.com/bphoto/6cnIoBmQyIFAtwCghNODDg/ls.jpg", location:"710 W 5th St Austin, TX 78701 Market District, Downtown", reviews: {name: "Amy D.", img:"https://s3-media2.fl.yelpcdn.com/photo/CQXURdHlKIDOBRgw7BTVJw/60s.jpg", rating: '5', comment:"Man, the doctors and staff here are amazing. Every one of them are resopnsive, compassionate, and passionate! I really can't say enough great things about AUVC. I don't live nearby anymore, but I would dream of taking my pup anywhere else."}},
        
    ];

    let sheltersInfo = [
        {name: "Austin Dog Rescue", hours: ["Mon	9:00 am - 4:00 pm",
        "Tue	9:00 am - 4:00 pm",	
        "Wed	9:00 am - 4:00 pm",	
        "Thu	9:00 am - 4:00 pm",
        "Fri	9:00 am - 4:00 pm",	
        "Sat	9:00 am - 4:00 pm",	
        "Sun	9:00 am - 4:00 pm"],contact:"(512) 827-9787", img: "https://s3-media4.fl.yelpcdn.com/bphoto/oDPwa3LvjhjEaur87Dunrw/o.jpg", location:"Austin, TX", rating: "4.5", category: ["Animal Shelters", "Pet Adoption"], reviews: {name: "Candice", rating: "5", img: "https://s3-media3.fl.yelpcdn.com/photo/fUhezXkqxzKBv1vfVJKj9w/60s.jpg", comment: "ADR is the best. We looked all over when we decided to get a dog and couldn't be happier with our ADR experience. They do amazing work finding dogs who might otherwise be put down. The volunteers are dedicated and helpful. They met with us and then brought dogs to our place to test out. The foster has stayed in touch and helped us with questions."}},
        {name: "Kate To The Rescue Animal Rescue", hours: ["Mon	9:00 am - 4:00 pm",
        "Tue	9:00 am - 4:00 pm",	
        "Wed	9:00 am - 4:00 pm",	
        "Thu	9:00 am - 4:00 pm",
        "Fri	9:00 am - 4:00 pm",	
        "Sat	9:00 am - 4:00 pm",	
        "Sun	9:00 am - 4:00 pm"],contact:"katetotherescue.org", img:"https://s3-media2.fl.yelpcdn.com/bphoto/XLw3zMOZ7TIcXxW5jsIBNg/o.jpg", location:"Austin, TX", category: "Animal Shelters", reviews: {name: "Westlake Animal Hospital", rating: "4.5", img: "https://s3-media1.fl.yelpcdn.com/photo/2NZQ0CymsVgN1twjUFrIrg/60s.jpg", comment: "I've been to this facility twice in the last few months. Having worked in the veterinary field for over 10 years, saying I'm picky about who cares for my beloved frenchie is an understatement. The fact…"}},
        {name: "Austin Wildlife Rescue", hours: ["Mon	9:00 am - 4:00 pm",
        "Tue	9:00 am - 4:00 pm",	
        "Wed	9:00 am - 4:00 pm",	
        "Thu	9:00 am - 4:00 pm",
        "Fri	9:00 am - 4:00 pm",	
        "Sat	9:00 am - 4:00 pm",	
        "Sun	9:00 am - 4:00 pm"], contact:"(512) 472-9453", img: "https://s3-media3.fl.yelpcdn.com/bphoto/yrmDh-ueiSju9B5JwY64fg/ls.jpg", location:"5401 E Martin Luther King Blvd Austin, TX 78721", rating: "4", category: "Animal Shelters", reviews: {name: "Tracey S", rating: "5", img: "https://s3-media2.fl.yelpcdn.com/photo/KRRMyj6dQslyR49I1cdAdw/ls.jpg", comment: "Austin is lucky to have this organization of caring wildlife rehabbers and visiting veterinarians to take animals to. I've brought grackle fledglings and many ducks to them over the last few years.  People often abandon their unwanted ducks and geese at parks.  Some of them hadn't been taken care of properly and ended up permanently handicapped.  I generally tried to take those who were unable to walk or swim properly to Austin Wildlife Rescue.  People must bring in the injured/sick/orphaned wildlife; the rescue does not catch them, understandably"}}
        ,{name: "Austin Humane Society", hours: ["Mon	12:00 pm - 7:00 pm",
        "Tue	12:00 pm - 7:00 pm",	
        "Wed	12:00 pm - 7:00 pm",	
        "Thu	12:00 pm - 7:00 pm",
        "Fri	12:00 pm - 7:00 pm",	
        "Sat	12:00 pm - 7:00 pm",	
        "Sun	12:00 pm - 7:00 pm"], contact:"(512) 476-2882", img: "https://s3-media1.fl.yelpcdn.com/bphoto/uM9O_kuOfIF2EcAiyFMuZA/o.jpg", location:"124 W Anderson Ln Austin, TX 78752", rating: "4", category: "Animal Shelters", reviews: {name: "Cody S", rating: "5", img: "https://s3-media1.fl.yelpcdn.com/photo/GH-8g8I1mp7nNtxuJOE6Jg/60s.jpg", comment: "Austin Humane saved my life! Who rescued who, you know? Leo; the sweetest, bestest, smiliest, sensitivist, loviest puppo on the planet found us here. He had been unadopted or fostered for almost 6 months! He was a Hurricane Harvey survivor and ended up without a home like so many thousands of other animals. "}}
       
    ];


    let tempData = {};
    switch(dataType){
        case 'pet':
            tempData = petsInfo[Id];
            break;
        case 'vet':
            tempData = vetsInfo[Id];
            break;
        case 'shelter':
            tempData = sheltersInfo[Id];
            break;
        default:
            tempData = petsInfo[Id];
    };

   
    return function(dispatch){
        // TODO 
        //call axios with url to get data from backend

        dispatch({
            type: FETCH_PET_ENTRY,
            data: tempData
        });
    }
}

export function fetchData(dataType){
    // const baseUrl = ""
    //let url = baseUrl.concat(dataType);

    // temporary static data for phase 1
    let tempData = {};
    let pets = [
        {
            id: "0",
            name: "Charger",
            image: "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/38345765/2/?bust=1496895722",
            breed: "Siamese",
            sex: "Male",
            age: "Adult"
        },
        {
            id: "1",
            name: "June 2",
            image: "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/41998452/3/?bust=1529884412",
            breed: "Domestic Short Hair",
            sex: 'Female',
            age: "Adult"
        },
        {
            id: "2",
            name: "Shaggy",
            image: "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/42022417/1/?bust=1530117339",
            breed: "Shih Tzu",
            sex: "Male",
            age: "Adult"
        },
        {
            id: "2",
            name: "Spencer",
            image: "http://photos.petfinder.com/photos/pets/40889818/1/?bust=1518034088&width=500&-x.jpg",
            breed: "Beagle",
            sex: "Male",
            age: "Adult"
        }
    ];

    let vets = [
        {
            id: "0",
            name: "Emancipet",
            image: "https://s3-media3.fl.yelpcdn.com/bphoto/wLB-AA3Dt15HeHs1OOxGGQ/ls.jpg",
            location: "Austin",
            rating: "4",
            contact: "(512) 587-7729"
        },
        {
            id: "1",
            name: "Zippivet",
            image: "https://s3-media1.fl.yelpcdn.com/bphoto/S2LYJ96acZeO62hfcfzHjQ/ls.jpg",
            location: "Austin",
            rating: "4.5",
            contact: "(512) 904-0218"
        },
        {
            id: "2",
            name: "PAZ Veterinary",
            image: "https://s3-media4.fl.yelpcdn.com/bphoto/MvDi_2SFJk8WYtCdRCnIEA/o.jpg",
            location: "Austin",
            rating: "5",
            contact: "(512) 236-8000"
        },
        {
            id: "3",
            name: "Austin Urban Vet Center ",
            image: "https://s3-media1.fl.yelpcdn.com/bphoto/6cnIoBmQyIFAtwCghNODDg/ls.jpg",
            location: "Austin",
            rating: "4",
            contact: "(512) 476-2882"
        }
    ];
    
    let shelters = [
        {
            id: "2",
            name: "Austin Wildlife Rescue",
            image: "https://s3-media3.fl.yelpcdn.com/bphoto/yrmDh-ueiSju9B5JwY64fg/ls.jpg",
            location: "Austin",
            rating: "4.5",
            contact: "(512) 472-9453"
        },
        {
            id: "3",
            name: "Austin Humane Society",
            image: "https://s3-media1.fl.yelpcdn.com/bphoto/uM9O_kuOfIF2EcAiyFMuZA/o.jpg",
            location: "Austin",
            rating: "4.0",
            contact: "(512) 646-7387"
        },
        {
            id: "0",
            name: "Austin Dog Rescue",
            image: "https://s3-media4.fl.yelpcdn.com/bphoto/oDPwa3LvjhjEaur87Dunrw/o.jpg",
            location: "Austin",
            rating: "4.5",
            contact: "(512) 827-9787"
        },
        {
            id: "1",
            name: "Kate To The Rescue Animal Rescue",
            image: "https://s3-media2.fl.yelpcdn.com/bphoto/XLw3zMOZ7TIcXxW5jsIBNg/o.jpg",
            location: "Austin",
            rating: "0",
            contact: "katetotherescue.org"
        }
    ];
    switch(dataType){
        case 'pet':
            tempData = pets;
            break;
        case 'vet':
            tempData = vets;
            break;
        case 'shelter':
            tempData = shelters;
            break;
        default:
            tempData = pets;
    };

    return function(dispatch){
        // TODO 
        //call axios with url to get data from backend

        dispatch({
            type: FETCH_DATA,
            data: tempData
        });
    }
}
