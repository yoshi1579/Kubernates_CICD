#!/usr/bin/env python3

from flask import Flask, jsonify
import random
# # from quotes import funny_quotes
# from quotes import random

app = Flask(__name__)

# @app.route('/')
# def serve_funny_quote():
#     quotes = funny_quotes()
#     num_quotes = len(quotes)
#     # selected_quotes = quotes[random.randint(0, num_quotes - 1)]
#     selected_quotes = random()
#     return jsonify(selected_quotes)

# if __name__ == '__main__':
#     app.run(debug=True)

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == '__main__':
    # app.run(debug=True,host='0.0.0.0')
    app.run(host='0.0.0.0')

# from flask import Flask, render_template, request, url_for, redirect, flash
# from flask import jsonify, request, send_file

# app = Flask(__name__)

# @app.route('/')
# def hello():
#     return "Hello World!"

# @app.route('/', methods=['GET', 'POST'])
# @app.route('/')
# def index():
#     return render_template('index.html')

# @app.route('/index')
# def index2():
#     return render_template('index.html')

# @app.route('/pets')
# def pets():
#     return render_template('pets.html')

# def hello_world():
#     return 'Flask Dockerized'

# def home():
#     return render_template('index.html')

# if __name__ == '__main__':
#     app.run(debug=True, host='0.0.0.0')
