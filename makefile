.DEFAULT_GOAL := all

all:
	kubectl create -f frontend-deployment.yaml
	kubectl expose deployments --port=80 --target-port=3000 --type=LoadBalancer greetings-deployment
